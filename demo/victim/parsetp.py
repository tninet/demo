#!/usr/bin/env python3
# coding: utf-8

import re
import argparse
import os
import sys
import datetime

def mean(numbers):
    return float(sum(numbers)) / max(len(numbers), 1)

def parse_strongswan_file(logfile):
    dates = {}
    tps = []
    with open(logfile,'r') as f:
        # count = 0
        for line in f:
            words = line.split()
            if len(words)<4:
                continue
            sanum = words[4].split("|")[-1].strip("<").strip(">")
            date = datetime.datetime.strptime(words[2], "%H:%M:%S.%f")
            if sanum in dates:
                datedb = dates[sanum][1]
                if dates[sanum][0] == "m1":
                    # print(sanum + " " + str(date - datedb))
                    tps.append((date - datedb).total_seconds())
                    dates[sanum][0] = "m2"
                elif dates[sanum][0] == "m2":
                    dates[sanum][1] = date
                    dates[sanum][0] = "m3"
                elif dates[sanum][0] == "m3":
                    # print(sanum + " " + str(date - datedb))
                    tps.append((date - datedb).total_seconds())
                    dates[sanum][0] = "m4"
            else:
                dates[sanum] = ["m1", date]
            # count += 1
            # if count > 6:
            #     break
        print("Average Tp: " + str(mean(tps)))
    return

def parse_libreswan_file(logfile):
    dates = {}
    tps = []
    with open(logfile,'r') as f:
        # count = 0
        for line in f:
            words = line.split()
            if len(words)<1:
                continue
            sanum = words[4].split("|")[-1].strip("<").strip(">")
            date = datetime.datetime.strptime(words[2], "%H:%M:%S.%f")
            if sanum in dates:
                datedb = dates[sanum][1]
                if dates[sanum][0] == "m1":
                    # print(sanum + " " + str(date - datedb))
                    tps.append((date - datedb).total_seconds())
                    dates[sanum][0] = "m2"
                elif dates[sanum][0] == "m2":
                    dates[sanum][1] = date
                    dates[sanum][0] = "m3"
                elif dates[sanum][0] == "m3":
                    # print(sanum + " " + str(date - datedb))
                    tps.append((date - datedb).total_seconds())
                    dates[sanum][0] = "m4"
            else:
                dates[sanum] = ["m1", date]
            # count += 1
            # if count > 6:
            #     break
        print("Average Tp: " + str(mean(tps)))
    return

def main():
    parser = argparse.ArgumentParser(description="""Parses the message process time Tp from
                                     an IKEv2 implementation log file logfile.

                                     For strongSwan,
                                     logfile\'s logging options must be:

                                     default = -1 
                                     net = 1
                                     ike_name = yes
                                     time_add_ms = yes
                                     time_format = %b %e %T

                                     For Libreswan, logs are taken from journalctl where
                                     microseconds are displayed:

                                     journalctl -o short-precise --no-pager -S "$starttime" \
                                     /usr/lib/ipsec/pluto > /vagrant/log/pluto.log
                                     """)
    parser.add_argument('logfile', nargs='?', type=str, default='log/tp.log',
                      help='path to log file to parse')
    parser.add_argument('implem', nargs='?', type=str, default='strongswan',
                      help='IKEv2 implementation from which logfile was taken')
    parser.add_argument('-v', '--verbosity', action='count', default=0)
    args = parser.parse_args()

    if args.implem == "strongswan":
        parse_strongswan_file(args.logfile)
    elif args.implem == "libreswan":
        parse_libreswan_file(args.logfile)
    else:
        print("Implementation not recognized")

    return

if __name__=='__main__':
    main()
