name="victim"

if [ ! -d "/vagrant" ]; then
    source "../shared/pre.sh"
else
    source "/vagrantshared/pre.sh"
fi

memlog="$main/log/mem.log"
daemonlog="$main/log/$daemon.log"
memsource="/sys/fs/cgroup/memory/$software"
memsource="$memsource/memory.usage_in_bytes"

reportnumconns() {
    local peer="$1"
    local count=0
    if [[ "$software" = strongswan ]]; then
        if local st="$($sudo timeout 5 ipsec status $peer)"
        then
            if local gr="$(grep "ESTABLISHED" <<< "$st")"; then
                if [[ -n "$gr" ]]; then
                    count="$(wc -l <<< "$gr")"
                fi
            fi
        fi
    elif [[ "$software" = libreswan ]]; then
        if local st="$($sudo timeout 5 ipsec whack \
            --trafficstatus 2> /dev/null)"; then
            if local gr="$(grep "$peer" <<< "$st")"; then
                if [[ -n "$gr" ]]; then
                    count="$(wc -l <<< "$gr")"
                fi
            fi
        fi
    fi
    echo "Number of connections with $peer: $count" >> $log
}

reportnumconnsall() {
    for i in $(seq 1 $initsize); do
        reportnumconns initiator$i
    done
    reportnumconns probe
    return 0
}
