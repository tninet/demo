#!/usr/bin/env bash

# If Victim is real, this file is to be executed directly in
# its folder.

# exec &> /dev/null
if [ ! -d "/vagrant" ]; then
    source "pre.sh"
else
    source "/vagrant/pre.sh"
fi


# lifetime=$(echo "1/$sigmaprobe" | bc)
ikelifetime="10s"
lifetime="10s"
margintime="5s"
# rekey=no
# ikelifetime=$ikelifetime
# lifetime=$lifetime
# margintime=$margintime
# rekeyfuzz=0%
if [[ "$nonpersistentprobe" = true ]]; then
    prefix=''
else
    prefix='#'
fi

set +x
if [ "$software" = "strongswan" ]; then
    conffile="$main/conf/strongswan/ipsec.conf"
    charonlogging="$main/conf/strongswan/charon-logging.conf"

    cat > $conffile <<-EOF
config setup
    # strictcrlpolicy=yes
    uniqueids = never

conn %default
    dpdaction=none
    dpddelay=0s

conn probe
    leftsubnet=10.1.0.0/16
    left=192.168.33.20
    leftcert=victim.crt
    leftid="CN=victim"
    rightsubnet=10.1.0.0/16
    right=192.168.33.10
    rightid="CN=probe"
    ${prefix}inactivity=5s
    auto=add

EOF
    for i in $(seq 1 $initsize); do
        cat <<-EOF
conn initiator$i
    leftsubnet=10.0.0.0/8
    left=192.168.33.20
    leftcert=victim.crt
    leftid="CN=victim"
    rightsubnet=10.0.0.0/8
    right=192.168.33.3$i
    rightid="CN=initiator$i"
    dpdaction=clear
    auto=add

EOF
    done >> $conffile

    cat > $charonlogging <<-EOF
# charon-logging.conf
charon {
    filelog {
        # This log file allows us to get encryption keys
        # $main/log/charon.log {
        #     append = no
        #     default = -1
        #     ike = 4
        #     enc = 3
        #     ike_name = yes
        #     time_add_ms = yes
        #     time_format = %b %e %T
        # }
        $main/log/tp.log {
            append = no
            default = -1
            net = 1
            ike_name = yes
            time_add_ms = yes
            time_format = %b %e %T
        }
    }
    syslog {
        daemon {
            default = 2
            mgr = 0
            net = 2
            enc = 1
            asn = 1
            job = 2
        }
    }
}
EOF

elif [ "$software" = "libreswan" ] || \
    [ "$software" = "openswan" ]; then
    if [ "$software" = "libreswan" ]; then
        conffile="$main/conf/$software/libre.conf"
        ext=""
    else
        conffile="$main/conf/$software/ipsec.conf"
        ext=".crt"
    fi

    rm $conffile

    if [ "$software" = "openswan" ]; then
        cat >> $conffile <<-EOF
config setup
    protostack=netkey
    uniqueids=no
    plutodebug=all
    plutostderrlog=$main/log/pluto.log

EOF
    fi

    cat >> $conffile <<-EOF
conn %default
    ikev2=insist
    left=192.168.33.20
    leftid="CN=victim"
    leftcert=victim$ext
    authby=rsasig
    auto=add

EOF

    for i in $(seq 0 $imax); do
        for j in $(seq 1 $jmax); do
            for k in $(seq 1 $initsize); do
                echo "\
conn initiator${k}_$(($i*$jmax+$j))
    leftsubnet=10.1.$(($k*($imax+1)+$i)).$j/32
    right=192.168.33.3$k
    rightsubnet=10.1.$(($k*($imax+1)+$i)).$j/32
    rightid="CN=initiator$k"
"
            done

            echo "\
conn probe$(($i*$jmax+$j))
    leftsubnet=10.1.$i.$j/32
    right=192.168.33.10
    rightsubnet=10.1.$i.$j/32
    rightid="CN=probe"
"
        done
    done >> $conffile
fi

source $shared/configure.sh
