#!/usr/bin/env bash

dir=`pwd`

source ../shared/democonf.sh

rm *.key *.csr *.crt index.txt* serial*
rm -rf certs newcerts private
mkdir certs newcerts private

rm -rf ../{shared,initiator,victim,probe}/crypto
mkdir ../{shared,initiator,victim,probe}/crypto

touch index.txt
echo 1000 > serial

openssl genrsa -out private/ca.key 4096
openssl req -subj /CN=ca -key private/ca.key -new -x509 -days 7300 -out certs/ca.crt
cp certs/ca.crt ../shared/crypto

echo "initsize=$initsize"
for i in $(seq 1 $initsize)
do
    openssl genrsa -out initiator$i.key 4096
    openssl req -config ca.conf -subj /CN=initiator$i -key initiator$i.key -new -out initiator$i.csr
    openssl ca -config ca.conf -batch -days 365 -in initiator$i.csr -out initiator$i.crt
    mv initiator$i.{key,crt} ../initiator/crypto/
done

openssl genrsa -out victim.key 4096
openssl req -config ca.conf -subj /CN=victim -key victim.key -new -out victim.csr
openssl ca -config ca.conf -batch -days 365 -in victim.csr -out victim.crt
mv victim.{key,crt} ../victim/crypto/

openssl genrsa -out probe.key 4096
openssl req -config ca.conf -subj /CN=probe -key probe.key -new -out probe.csr
openssl ca -config ca.conf -batch -days 365 -in probe.csr -out probe.crt
mv probe.{key,crt} ../probe/crypto/

rm *.csr
