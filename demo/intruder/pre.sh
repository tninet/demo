name="intruder"

if [ ! -d "/vagrant" ]; then
    source "../shared/pre.sh"
else
    source "/vagrantshared/pre.sh"
fi

devratelog="$main/log/deviationrate.log"
devcountlog="$main/log/deviationcount.log"

reportstats() {
    local numpackets
    numpackets=$(countpackets)
    echo "Stats report at $(date)" >> $log
    printf "Number of deviated m1 messages: " >> $log
    echo $numpackets >> $log
    # printf "Measured average sigma: " >> $log
}
