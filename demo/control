#!/bin/bash

verbose=0
while [[ "$1" =~ ^- && ! "$1" == "--" ]]; do
    case $1 in
        -v|--verbose)
            # Each -v argument adds 1 to verbosity.
            verbose=$((verbose + 1))
            ;;
        -q|--quiet)
            # Each -q argument removes 1 from verbosity.
            verbose=$((verbose - 1))
            ;;
    esac
    shift
done
if [[ "$1" == '--' ]]; then
    shift
fi

measuresofmfile="shared/measuresofm.sh"
shared=shared
source $shared/prepre.sh

if [ "$sourcedebug" = false ]; then
    exec > >(tee >(cat >&5)) 2>&1
    :
fi

# closingvm=probe
runlog=$main/run.log
measuremlog=$main/measurem.log
devlog=$main/intruder/log/deviationcount.log
vms=(intruder probe)
folders=(intruder probe initiator)
for i in $(seq 1 $initsize); do
    vms+=(initiator$i)
done
unset i
if [ "$victimreal" = false ]; then
    vms+=(victim)
    folders+=(victim)
fi


declare -A values measuresofm precision fields
declare -a keys runids ncycles

getindex() {
    # getindex value arrayname
    # Get index of value in array of name arrayname.
    local value=${1:?}
    local arrayname=${2:?}[@]
    local array i
    array=(${!arrayname})
    for i in "${!array[@]}"; do
        if [[ "${array[$i]}" = "${value}" ]]; then
            echo "${i}";
        fi
    done
}

elementin () {
  local e match="$1"
  shift
  for e; do [[ "$e" == "$match" ]] && return 0; done
  return 1
}

folderof() {
    # folderof vm
    # Prints the folder containing vm.
    # If vm=initiator, prints initiator.
    local vm=${1:?}
    if [[ $vm =~ initiator? ]]; then
        echo initiator
    else
        echo $vm
    fi
}

logfileof() {
    # logfileof vm type
    # Returns the path to the logfile of type $type of $vm.
    # $type defaults to log.
    local vm=${1:?}
    local type=${2:-log}
    echo "$main/$(folderof $vm)/log/$vm.$type"
}

reportvm() {
    # reportvm vm
    # Prints the log of vm to stdout.
    local vm=${1:?}
    echo
    echo "= $vm log report ="
    cat $(logfileof $vm)
    echo
    echo "= $vm err report ="
    cat $(logfileof $vm err)
}

reportvms() {
    # reportvms
    # Prints the log of all vms to stdout.
    for vm in ${vms[@]}; do
        reportvm $vm
    done
}

controlvm() {
    # controlvm mode vm x [opt]...
    # If mode = run: runs "vagrant x opt..." in the folder of
    # vm.
    # If mode = prov: runs the provisioner x on vm.
    # If mode = provsub: runs the provisioner x on vm, where
    # vm is a subvm.
    local mode=${1:?}
    local vm=${2:?}
    local x=${3:?}
    local folder
    shift 3
    folder=$(folderof $vm)
    (
    cd $main/$folder
    case $mode in
        run)
            yell "Executing in $folder folder: vagrant $x $@"
            vagrant $x $@
            ;;
        prov)
            yell "Running the $x provisioner on $vm"
            vagrant provision --provision-with $x
            ;;
        provsub)
            yell "Running the $x provisioner on $vm"
            vagrant provision --provision-with $x $vm
            ;;
    esac
    )
}

controlvms() {
    # controlvms mode x [opt]...
    # If mode = run: runs "vagrant x opt..." in all folders.
    # If mode = prov: runs the provisioner x on all vms.
    local mode=${1:?}
    local x=${2:?}
    shift 2
    for folder in "${folders[@]}"; do
        controlvm $mode $folder $x $@
    done
}

provinitpar() {
    # provinitpar x
    # Runs the x provisioner on initiator subvms in parallel.
    local x=${1:?}
    local i
    if [[ $initsize -gt 1 ]]; then
        for i in $(seq 1 $((initsize - 1))); do
            controlvm provsub initiator$i $x &
        done
    elif [[ $initsize -lt 1 ]]; then
        die initsize is too low
    fi
    controlvm provsub initiator$initsize $x
}

startexp() {
    configure rundebug false $runconf
    configure sourcedebug false $runconf
    controlvms prov start
    setsid $main/display &> /dev/null &
    yell "Waiting for connection configurations to get loaded"
    if [ "$software" = "strongswan" ]; then
        sleep 5
    else
        sleep 40
    fi
    controlvm prov probe connect
}

stopexp() {
    # In order to only record a DoS during the configured
    # duration, stop Probe first.
    # In order to display the number of connections in Victim
    # with Probe, stop Victim first. However, this is only
    # possible when Victim is not in memory exhaustion when
    # you stop it, since ipsec status does not give any
    # result when Victim is in DoS.
    controlvm prov probe stop
    if [ "$victimreal" = false ]; then
        controlvm prov victim stop
    fi
    controlvm prov intruder stop
    # provinitpar stop
    controlvm prov initiator stop
    # [ "$software" == "strongswan" ] && ./getkeys.sh
    # for folder in "intruder" "victim"; do
    yell "Writing run results to run.log"
    reportvms > $runlog 2>&1
    yell
    if [ -f "$(logfileof probe)" ]; then
        grep -A 1 "DoS detected" "$(logfileof probe)" || \
            echo "No DoS detected"
    fi
}

attack() {
    provinitpar connect
}

chooserunduration() {
    local theorytmem
    local capa=${1:?}
    local sig=${2:?}
    local m=${3:-${!mrefopt}}
    # In practice, measured sigma can be up to 1.2 times lower
    # than configured sigma. Therefore, expected tmem and
    # measured tmem can be up to 1.2 times lower than theory
    # tmem. We thus stop the machines after 1.5 * theory tmem.
    theorytmem=$(calculatetmem $capa ${!lopt} $m $sig)
    echo "$theorytmem * 1.4 / 1 + 60" | bc
}

estimatemeasuremtime() {
    local capa=${1:?}
    local throughput=${2:?}
    local numconns
    numconns=$(echo "($capa - ${!lopt}) / ${!mrefopt}" | bc)
    echo $((numconns / throughput))
}

boot() {
    controlvms run up
}

halt() {
    controlvms run halt
}

setup() {
    rm -rf {initiator,victim,intruder,probe}/log
    mkdir -p {initiator,victim,intruder,probe}/log
    (
    cd $main/ca
    ./genpki.sh
    )
    cp $shared/runconf.sh{.template,}

    # Use this to keep your logs across reboots:
    # configure Storage persistent /etc/systemd/journald.conf

    controlvms run up
}

install() {
    controlvms prov install
}

uninstall() {
    controlvms prov uninstall
}

switchtosoftware() {
    local software=${1:?}
    local softwares=(strongswan libreswan)
    if ! elementin $software "${softwares[@]}"; then
        yell "$software is not supported"
        return 1
    fi
    uninstall
    configure software $software $runconf
    install
    return 0
}

destroy() {
    controlvms run destroy -f
}

updatenicsingle() {
# Update network connections of $vm after a change from a
# real victim to a virtual one and vice versa. This is
# needed e.g. if the change was made without destroying the
# VMs.
    vm="$1"
    if [ "$victimreal" = true ]; then
        vboxmanage controlvm "$vm" nic2 bridged "$hostiface"
    else
        vboxmanage controlvm "$vm" nic2 hostonly vboxnet1
    fi
}

updatenicall() {
# Update network connections of all VMs
    local i
    for folder in ${monofolders[@]}; do
        updatenicsingle "$folder"
    done
    for i in $(seq 1 $initsize); do
        updatenicsingle "initiator$i"
    done
}

printfunctions() {
    # declare -f
    declare -F | awk '{print $3}' | xargs
}

installzshcompletion() {
# After that you need to run this in your shell:
# unfunction _control
# autoload -U _control
    local shellrc="$HOME/.zshrc"
    if ! grep "Deviation attack" "$shellrc" \
        &>/dev/null; then
        IFS='' read -d '' str << EOF
# Deviation attack demo completion
fpath+=($main)
EOF
        sed -i "1s/^/$str\n/" $shellrc
    fi
}

getresult() {
    local starttime dostime starttimesec dostimesec
    local key=${1:?}
    if [[ $key = "deviated" ]]; then
        # grep "Average measured sigma:" $runlog | \
            # awk '{printf("%3.1f",$4)}'
        grep "Number of deviated m1 messages" $runlog \
            | tail -n 1 \
            | cut -d ' ' -f 6
    elif [[ $key = "mt" ]]; then
        starttime=$(grep -A 4 "initiator1 log" $runlog | \
            tail -n 1)
        dostime=$(grep -m 1 -A 1 "DoS detected at:" \
            $runlog | tail -n 1) \
            || die No DoS detected
        starttimesec=$(date -u -d "$starttime" +"%s")
        dostimesec=$(date -u -d "$dostime" +"%s")
        echo $((dostimesec - starttimesec))
    elif [[ $key = "ml" ]]; then
        grep "Memory report:" $runlog | \
            awk '{printf("%3.1f",$3/1000000)}'
    fi
}

round() {
    # round value key
    # Rounds value using precision of key and echoes the
    # result.
    local value=${1:?}
    local key=${2:?}
    python3 << EOF
precision=${precision[$key]}
if precision!=0:
    print(round($value,precision))
else:
    print(round($value))
EOF
}

adapttodisplay() {
    # adapattodisplay value key
    # Adapt value to display of key type and echoes the
    # result.
    local value=${1:?}
    local key=${2:?}
    if [[ $key = m ]]; then
        value=$(python3 << EOF
print($value*1000)
EOF
        )
    fi
    round $value $key
}

calculatetmem() {
    local c l m s res
    c=${1:?}
    l=${2:?}
    m=${3:?}
    s=${4:?}
    if [[ $verbose -gt 0 ]]; then
        yell "
c=$c
l=$l
m=$m
s=$s"
    fi
    res=$(echo "($c - $l) / ($m * $s)" | bc)
    if [[ $verbose -gt 0 ]]; then
        yell "res=$res"
    fi
    echo $res
}

setrelativeerror(){
    # For each id, if values[re,id,cid] is not set, calculate
    # relative error between values[et,id,cid] and
    # values[mt,id,cid] and put it in values[re,id,cid].
    local id
    for id in ${runids[@]}; do
        if [[ -z ${values[re,$id,$cid]:-} ]]; then
            values[re,$id,$cid]=$(python3 << EOF
print((abs(${values[et,$id,$cid]}-${values[mt,$id,$cid]})) \
    /${values[et,$id,$cid]}*100)
EOF
)
        fi
    done
}

setcolumnstring() {
    # Sets columnstring
    local count tmp
    # columnstring=$(printf 'c|%.0s' $(seq 1 ${#runids[@]}))
    columnstring=""
    tmp=0
    for id in ${runids[@]}; do
        if [[ ${values[c,$id]} = $tmp ]] || [[ $tmp = 0 ]]; then
            columnstring+='c|'
        else
            columnstring+='|c|'
        fi
        tmp=${values[c,$id]}
    done
}

subtable() {
    # Append $string to line containing $key in $table.
    # Return modified table.
    local table=${1:?}
    local key=${2:?}
    local string=${3:?}
    local sub
    sub='/^'
    # if [[ $key = re ]]; then
        # Replace \ with \\ so that \ is escaped in sed.
        # sub+="${fields[$key]/\\/\\\\}"
    # else
        # sub+="${fields[$key]}"
    # fi
    sub+="${fields[$key]}"
    sub+='/s/\\\\/'
    sub+="$string"
    sub+='\\\\/'
    sed "$sub" <<< "$table"
}

estimatetime() {
    local dur time id
    setconfvalues
    time=0
    if [[ $warmuprun = true ]]; then
        id=$warmuprunid
        dur=$(chooserunduration ${values[c,$id]} \
            ${values[cs,$id]})
        time=$((time + dur))
    fi
    for id in ${runids[@]}; do
        dur=$(estimatemeasuremtime ${values[c,$id]} \
            ${values[cs,$id]})
        time=$((time + dur))
        dur=$(chooserunduration ${values[c,$id]} \
            ${values[cs,$id]})
        time=$((time + ncycles * dur))
    done
    if [[ "$software" = libreswan ]]; then
        time=$((time + ncycles * ${#runids[@]} * 80))
    fi
    yell "Generating the whole table will take \
$(convertsecs time)"
}

debugconns() {
    local sigma=${1:?}
    local m=${2:-${!mrefopt}}
    local text startdate reportdate reportime numconns \
        nummessages
    startdate=$(\
        sed -n '/initiator1 log/,/initiator2 log/ p' $runlog \
        | grep -A 1 "Connecting at:" \
        | tail -n 1)
    text=$(\
        sed -n '/victim log/,/victim err/ p' $runlog \
        | grep -m 1 -A $((initsize+3)) "Stats report at")
    reportdate=$(head -n 1 <<<"$text" \
        | cut -d ' ' -f 4-9)
    reporttime=$(($(date -u -d "$reportdate" +"%s") \
        - $(date -u -d "$startdate" +"%s")))
    numconns=$(grep "Number of connections with initiator" \
        <<< "$text" \
        | awk '{sum += $6} END {print sum}')
    # numreceivedmess=$(\
        # grep "Number of received m1 messages" <<< "$text" \
        # | cut -d ' ' -f 6)
    numdeviatedmess=$(\
        grep -m 1 "Number of deviated m1 messages" $runlog \
        | cut -d ' ' -f 6)
    memprecision=3
    mem=$(\
        grep "Memory report:" <<< "$text" \
        | cut -d ' ' -f 3)
    mem=$(python3 <<EOF
print(round($mem/10**6,$memprecision))
EOF
)
    thmem=$(python3 <<EOF
print(round(${!lopt}+$sigma*$reporttime*$m,$memprecision))
EOF
)
# measured number of received m1 messages: $numreceivedmess
    yell "\
Tracking the number of connections at ${reporttime}s
sigma*$reporttime: $(python -c \
        "print(int($sigma * $reporttime))")
measured number of deviated m1 messages: $numdeviatedmess
measured number of installed connections: $numconns
measured occupied memory: $mem MB
occupied memory should be: $thmem MB
"
}

testdebugconns() {
    controlvm prov victim start
    controlvm prov intruder start
    controlvm provsub initiator1 start
    controlvm provsub initiator1 manualconn
    controlvm prov victim reportstats
    controlvm prov victim stop
    controlvm provsub initiator1 stop
    reportvms > $runlog 2>&1
    # measuremthroughput=10
    debugconns $measuremthroughput
}

getdeviatedat() {
    # getdeviatedat time
    # Returns the number of deviated m1 messages at time.
    local time=${1:?}
    python3 << EOF
devlog="$devlog"
with open(devlog,"r") as f:
    lines=f.readlines()
    records=[[int(f) for f in line.split()] \
        for line in lines]
starttime=min([rec[0] for rec in records if rec[1]!=0])
time=starttime+$time
for i in range(len(records)):
    diff=abs(records[i][0]-time)
    if i==0 or diff<mindiff:
        mindiff=diff
        indmin=i
a=records[indmin][1]
b=records[indmin+1][1]
timea=records[indmin][0]
timeb=records[indmin+1][0]
progress=(time-timea)/(timeb-timea)
print(round(a+progress*(b-a)))
EOF
}

measurem() {
    # measurem capa throughput sender
    # Measures the precise value of m when Victims memory
    # occupation is capa. Puts the result in global variable
    # m.
    # Use a measuremthroughput of throughput.
    # Sender may be one of: probe, one of the initiators,
    # initiators.
    local initialmem finalmem lines numconns
    local capa=${1:?}
    local throughput=${2:?}
    local sender=${3:-initiators}
    local sub
    m=""
    # We use the reference value of m in order to estimate
    # the number of connections that will be needed to cause a
    # memory exhaustion in Victim. We then measure m more
    # precisely when this amount of connections are set in
    # Victim.
    numconns=$(echo "($capa - ${!lopt}) / ${!mrefopt}" | bc)
    if [[ $verbose -gt 0 ]]; then
        yell '$numconns='$numconns
        yell '$throughput='$throughput
    fi
    # We need to set a capacity high enough so that memory
    # exhaustion does not falsify the measure of m.
    configure capacity $((5 * capa)) $runconf
    configure measuremthroughput $throughput $runconf
    configure imax $(python -c \
        "print(int($numconns/250+5))") $runconf
    if [[ $sender = initiators ]]; then
        configure measuremnumconns $numconns $runconf
    elif [[ $sender = probe ]]; then
        configure nonpersistentprobe false $runconf
        configure measuremnumconns $numconns $runconf
    elif [[ $sender =~ initiator[1-$initsize] ]]; then
        configure measuremnumconns $((numconns*3)) $runconf
    else
        die $sender is not a valid sender
    fi

    # Starting the VMs
    controlvm prov victim start
    if [[ $sender = initiators ]]; then
        controlvm prov initiator start
        controlvm prov intruder start
    elif [[ $sender = probe ]]; then
        controlvm prov probe start
    else
        controlvm provsub $sender start
        controlvm prov intruder start
    fi

    if [ "$software" = "strongswan" ]; then
        sleep 2
    else
        yell sleep 40
        sleep 40
    fi
    controlvm prov victim reportmemory
    yell "This will take about \
$(($numconns / $throughput)) seconds"

    # Installing connections
    if [[ $sender = initiators ]]; then
        provinitpar manualconn
    elif [[ $sender = probe ]]; then
        controlvm prov probe manualconn
    else
        controlvm provsub $sender manualconn
    fi

    yell "Waiting for all connections to be properly installed"
    sleep 10
    controlvm prov victim reportmemory

    # Stopping the VMs
    controlvm prov victim stop
    if [[ $sender = initiators ]]; then
        controlvm prov initiator stop
        controlvm prov intruder stop
    elif [[ $sender = probe ]]; then
        controlvm prov probe stop
    else
        controlvm provsub $sender stop
        controlvm prov intruder stop
    fi

    reportvm victim > $measuremlog

    lines="$(grep "Memory report:" $measuremlog)"
    lines=$(awk '{print $3}' <<< "$lines")
    IFS=$'\n' lines=($lines)
    initialmem=${lines[0]}
    finalmem=${lines[1]}
    m=$(echo "scale=6; ($finalmem - $initialmem) / ($numconns * 1000000)" | bc)
    
    if [[ -n $m ]]; then
        yell "Measure of m: $m MB."
    else
        yell "Measure of m failed."
    fi
    storemeasureofm $software $capa $throughput $m
}

storemeasureofm() {
    # storemeasureofm software c cs m
    # Stores the measure of m for software, c and cs in
    # measuresofmfile.
    local software=${1:?}
    local c=${2:?}
    local cs=${3:?}
    local m=${4:?}
    local key
    key="measuresofm[$software,$c,$cs]"
    configure $key $m $measuresofmfile
    return 0
}

run() {
    local duration=${1:-$duration}
    configure nonpersistentprobe true $runconf
    startexp
    # controlvm prov victim reportmemory
    attack
    (
    sleep $(python -c "print($duration/3)")
    controlvm prov victim reportstats &
    controlvm prov intruder reportstats
    ) &
    yell "Sleeping for $duration seconds"
    # Actually, since the time to stop the attack is not
    # negligible, the attack lasts a little longer than
    # duration.
    sleep "$duration"
    stopexp
}

causememex() {
    # causememex capa sig [m]
    # Causes a memory exhaustion on Victim with Victim being
    # granted a capacity of capa MB and using a throughput of
    # deviated m1 messages of sig messages/s. m is used to not
    # set imax too low. If m is not given, reference m is
    # taken from demo.conf.
    local capa=${1:?}
    local sig=${2:?}
    local m=${3:-${!mrefopt}}
    configure capacity $capa $runconf
    configure sigma $sig $runconf
    configure imax $(python -c \
        "print(int($capa/$m/250+5))") $runconf
    run $(chooserunduration $capa $sig $m)
}

setconfvalues() {
    # All keys will be displayed in table.
    keys=(c cs ms et mt re)
    source shared/gentableconf.sh
    if [[ $displaymintable = true ]]; then
        keys+=(m)
    fi
    # Do not modify these, as they will be matched with the
    # table template we use in code.
    # We escaped \ and $ because these strings are for feeding
    # sed.
    fields[c]='\$C\$'
    fields[cs]='Configured \$\\sigma\$'
    fields[m]='Measured \$m\$ (in kB)'
    fields[ms]='Measured \$\\sigma\$'
    fields[et]='Expected \$T^s_{mem}\$'
    fields[mt]='Measured \$T^s_{mem}\$'
    fields[re]='Relative error in \\%'
    if [[ -f shared/measuresofm.sh ]]; then
        source shared/measuresofm.sh
    fi
}

writetable() {
    # Write the result table of values[@,@,cid] using
    # available results. If only part of the experiments have
    # been run, then only these run ids will be displayed in
    # the table.
    local cid=${1:?}
    local -A strings
    local table key value id strtoadd
    setrelativeerror
    for key in ${keys[@]}; do
        strings[$key]=""
        for id in ${runids[@]}; do
            if [[ $key =~ c|cs ]]; then
                value=${values[$key,$id]:-}
            else
                value=${values[$key,$id,$cid]:-}
            fi
            if [[ -n $value ]]; then
                value=$(adapttodisplay $value $key)
            fi
            strings[$key]+="\& $value "
        done
    done

    {
        IFS='' read -r -d '' table <<"EOF"
\documentclass{article}
\begin{document}
\begin{table*}[!t]
\renewcommand{\arraystretch}{1.3}
\caption{Calculating and measuring $T^s_{mem}$ during some
Deviation Attacks against}
\label{table:resultstmem}
\centering
\begin{tabular}{|c||}
\hline
$C$ (in MB) \\
\hline
Configured $\sigma$ (in m1 messages/s) \\
\hline
Measured $\sigma$ (in m1 messages/s) \\
\hline
Expected $T^s_{mem}$ (in s) \\
\hline
Measured $T^s_{mem}$ (in s) \\
\hline
Relative error in \% \\
\hline
\end{tabular}
\end{table*}
\end{document}
EOF
    } || true
    if [[ $displaymintable = true ]]; then
        strtoadd='\\\hline\nMeasured $m$ (in kB) \\\\'
        table=$(sed "/${fields[cs]}/ a $strtoadd" \
            <<< "$table")
    fi
    for key in ${keys[@]}; do
        # Word splitting is not performed during assignment so
        # no need to quote the command substitution here.
        table=$(subtable "$table" $key "${strings[$key]}")
    done
    # table=$(subtable "$table" m "${strings[m]}")
    table=$(sed "s/against}/against $software}/" \
        <<< "$table")
    setcolumnstring
    table=$(sed "s/{|c||/{|c||$columnstring/" \
        <<< "$table")
    yell "Writing table"
    echo "$table" > results/table$cid.tex
}

loadresult() {
    # loadresult id cid
    # Extracts the result of experiment $id from latex result
    # file $main/results/table$cid.tex
    local id=${1:?}
    local cid=${2:?}
    local file idpos tmpstr
    file=$main/results/table$cid.tex
    # idpos is the position of id in the runids array.
    # Beware! This only works if table was written using the
    # same runids array.
    idpos=$(getindex $id runids)
    for key in ms et mt; do
        # Get the value at key key and idpos position and put
        # it in values[key,id,cid].
        # We take field number idpos+2 because:
        #   * we skip the first field (add 1)
        #   * cut starts counting the fields at 1 whereas
        #     getindex starts counting at 0 (add 1 again)
        tmpstr=$(grep -s "${fields[$key]}" $file \
            | cut -s -d '&' -f $((idpos+2))) || return 1
        # We remove string '\\' (if present) that is at the
        # end of the latex result lines.
        values[$key,$id,$cid]=${tmpstr%%'\\'}
        # If what we get is empty, it means that the result is
        # not available. We use [] because we want word
        # splitting to be performed here.
        if [ -z ${values[$key,$id,$cid]} ]; then
            return 1
        fi
    done
    return 0
}

testloadresult(){
    local id cid
    id=8
    cid=3
    setconfvalues
    try loadresult $id $cid
    echo ${values[ms,$id,$cid]}
}

setresvalues() {
    # For a given id and a given cid, sets values[key,id,cid]
    # where key~ms|et|mt.
    local id=${1:?}
    local cid=${2:?}
    local sum storedm c cs
    if [[ $verbose -gt 0 ]]; then
        yell "id=$id"
    fi
    c=${values[c,$id]}
    cs=${values[cs,$id]}
    storedm=${measuresofm[$software,$c,$cs]:-}
    if [[ $norun = true ]] || [[ $donotmeasurem = true ]]; then
        m=${!mrefopt}
    elif [[ -z $storedm ]]; then
        yell "\
Measuring m for experiment $id in cycle $cid"
        # This sets m
        measurem ${values[c,$id]} $cs
        measuresofm[$software,$c,$cs]=$m
    else
        m=$storedm
    fi
    if [[ $norun = true ]]; then
        values[mt,$id,$cid]=167
        values[ms,$id,$cid]=9.9876
    else
        # This is to display an m-correct theoritical line
        # on victim's memory graph (not a sigma-correct
        # however, since measured sigma will certainly be
        # different from configured sigma).
        configure m$software $m $runconf
        yell "Running experiment $id in cycle $cid"
        # This runs the experiment $id $cid
        causememex ${values[c,$id]} ${values[cs,$id]} $m
        if ! values[mt,$id,$cid]=$(getresult mt); then
            return 1
        fi
        values[ms,$id,$cid]=$(python3 -c "print( \
            $(getdeviatedat ${values[mt,$id,$cid]}) \
            / ${values[mt,$id,$cid]})")
    fi
    values[et,$id,$cid]=$(calculatetmem \
        ${values[c,$id]} ${!lopt} $m \
        ${values[ms,$id,$cid]})
    values[m,$id,$cid]=$m
    debugconns ${values[ms,$id,$cid]} $m
    if [[ $verbose -gt 0 ]]; then
        yell "
values[c,$id]}=${values[c,$id]}
values[cs,$id]=${values[cs,$id]}
values[ms,$id,$cid]=${values[ms,$id,$cid]}
values[et,$id,$cid]=${values[et,$id,$cid]}
values[mt,$id,$cid]=${values[mt,$id,$cid]}
runids=(${runids[@]})"
    fi
}

calculateaveragem() {
    # Calculates the average of measuresofm[$software,$c,$cs]
    # for c in values[c,@] and cs in values[cs,@].
    # Put the result in measuresofm[$software,avg].
    # averagemeasuredm=${measuresofm[$software,$c,$cs]:-}
    local sum c cs
    sum=0
    for id in ${runids[@]}; do
        c=${values[c,$id]}
        cs=${values[cs,$id]}
        sum=$(python3 << EOF
print($sum+${measuresofm[$software,$c,$cs]})
EOF
        )
    done
    measuresofm[$software,avg]=$(python3 << EOF
print($sum/${#runids[@]})
EOF
    )
}

calculateaveragerelativeerror() {
    # Perform an average over values[re,id,avg] with id in
    # runids and put the result in avgre.
    local sum
    sum=0
    for id in ${runids[@]}; do
        sum=$(python3 << EOF
print($sum+${values[re,$id,avg]})
EOF
        )
    done
    avgre=$(python3 << EOF
print($sum/$ncycles)
EOF
    )
}

calculateaverage() {
    # Calculates the average of values[key,id,@] and put it in
    # values[key,id,avg].
    local key=${1:?}
    local id=${2:?}
    local sum
    sum=0
    for cid in $(seq 1 $ncycles); do
        if [[ $verbose -gt 0 ]]; then
            yell "
key=$key
id=$id
cid=$cid
values[key,id,cid]=${values[$key,$id,$cid]}"
        fi
        sum=$(python3 << EOF
print($sum+${values[$key,$id,$cid]})
EOF
        )
    done
    values[$key,$id,avg]=$(python3 << EOF
print($sum/$ncycles)
EOF
    )
    if [[ $verbose -gt 0 ]]; then
        yell "
values[key,id,avg]=${values[$key,$id,avg]}"
    fi
}

gentable() {
    # Run experiments and generate the latex result table
    local id res m sum c cs mforet mtodisplay
    setconfvalues
    if [[ $warmuprun = true ]] && [[ $norun = false ]]; then
        id=$warmuprunid
        yell "Doing a warm up run using run id $id"
        causememex ${values[c,$id]} \
            ${values[cs,$id]} ${!mrefopt}
    fi
    for cid in $(seq 1 $ncycles); do
        for id in ${runids[@]}; do
            # Try to load result of experiment $id from
            # $main/results/table$cid.tex. If it succeeds,
            # continue. Otherwise, run experiment and write
            # table.
            if [[ $overwriteresults = false ]] && \
                loadresult $id $cid; then
                yell "Loading result for run $id of cycle $cid"
                continue
            fi
            # This runs the experiment $id $cid. It sets m, values[ms,id,cid],
            # values[et,id,cid] and values[mt,id,cid]
            setresvalues $id $cid
            # Only the last written table is complete.
            writetable $cid
        done
    done
    if [[ $donotmeasurem = false ]]; then
        # This sets measuresofm[$software,avg].
        calculateaveragem
    fi
    for id in ${runids[@]}; do
        # This sets values[ms,id,avg].
        calculateaverage ms $id

        # This sets values[mt,id,avg].
        calculateaverage mt $id

        c=${values[c,$id]}
        cs=${values[cs,$id]}
        if [[ $donotmeasurem = false ]]; then
            values[m,$id,avg]=${measuresofm[$software,$c,$cs]}
        else
            values[m,$id,avg]=${!mrefopt}
        fi
        if [[ $etisavg = true ]]; then
            # This sets values[et,id,avg].
            calculateaverage et $id
        else
            if [[ $donotmeasurem = true ]]; then
                mforet=${!mrefopt}
            elif [[ $useaveragem = true ]]; then
                mforet=${measuresofm[$software,avg]}
            else
                mforet=${measuresofm[$software,$c,$cs]:-}
            fi
            values[et,$id,avg]=$(calculatetmem \
                ${values[c,$id]} \
                ${!lopt} $mforet \
                ${values[ms,$id,avg]})
        fi
    done
    writetable avg
    if [[ $donotmeasurem = false ]]; then
        mtodisplay=$(adapttodisplay \
            ${measuresofm[$software,avg]} m)
        yell "average m = $mtodisplay kB"
    fi
    calculateaveragerelativeerror
    yell "average relative error = $(adapttodisplay \
        $avgre re)%"
    return 0
}

$@
exit 0
