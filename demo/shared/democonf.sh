#!/usr/bin/env bash

#==========================
# Demo configuration file
#==========================

# IKEv2 implementation used. Accepted values are strongswan,
# libreswan and openswan.
software=strongswan

# Time after which Probe considers it has been denied a
# service (in s)
# tacc=10
tacc=5

# Number of Initiator machines to instantiate (corresponds the
# paper's Ndemo)
# Maximum 9
initsize=3

# This is the L variable defined in the paper. It is used to
# display the theoretical evolution of memory in Victim's
# memory graph. You can measure load on Victim by looking at
# the amount of memory occupied by software before there is
# any connection set up.
# These values also serve as a reference during gentable.
# Unit is MB.
loadstrongswan=1
loadlibreswan=18

# This is the m variable defined in the paper: the memory size
# of one connection. This is used to display the theoretical
# evolution of memory in Victim's memory graph. We describe
# how we measure m in the paper.
# The ref values serve as a reference during gentable.
# Unit is MB.
mstrongswanref=0.0188
mlibreswanref=0.053

# Throughput of m1 messages sent by Probe (in m1/s).
sigmaprobe=0.5

# Number of failures that are allowed by Probe before
# declaring a DoS. If Probe fails strictly more than tolerence
# times to set up a connection with Victim, then Probe
# declares a DoS.
tolerence=2

# If correctts is true, then the Initiators will send a TS in
# their m3 messages that is acceptable to Victim. A connection
# will thus consist of one IKE SA and one Child SA. Otherwise
# a connection consists of only one IKE SA.
correctts=false

# Average time needed by Victim to process one IKEv2 message
# (in s). This is used to display the theoretical Tp(t) on the
# Tp graph. You can measure it on Victim using the parsetp.py
# script
tp=0.0144

# Name of network interface used by the host machine to bridge
# all VMs in case Victim is real
hostiface="enp0s31f6"

# Whether Victim is a VM or a real machine
victimreal=false

# Name of network interface used by Victim for IKEv2 in case
# Victim is real
victimiface="eth0"
