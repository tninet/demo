#!/usr/bin/env bash

showsoftlogs() {
    showlogs $daemon
}
showlogs() {
    local pos starttime
    local id=${1:-$daemon}
    if [ "$software" = "strongswan" ]; then
        pos=4
    elif [ "$software" = "libreswan" ]; then
        pos=5
    fi

    starttime=$(grep -A 1 "Starting at:" $log | tail -n1 | \
        awk "{print \$${pos}}")
    # starttime=$(grep -A 1 "Starting at:" $log | tail -n1)

    # yell $id
    $sudo journalctl -o short-precise -S "$starttime" \
        SYSLOG_IDENTIFIER=$id
}
