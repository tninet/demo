#!/usr/bin/env bash

# exec &> /dev/null

if [ "$software" = "strongswan" ]; then
    cat > $main/conf/strongswan/ipsec.secrets << EOF
: RSA $name.key
EOF

    $sudo cp $main/conf/strongswan/ipsec.{conf,secrets} /etc/
    $sudo cp \
        $main/conf/strongswan/{charon,charon-logging}.conf \
        /etc/strongswan.d/
    $sudo cp $shared/crypto/ca.crt /etc/ipsec.d/cacerts/
    $sudo cp $main/crypto/$name.key /etc/ipsec.d/private
    $sudo cp $main/crypto/$name.crt /etc/ipsec.d/certs
elif [ "$software" = "libreswan" ]; then
    # rm /var/lib/ipsec/nss/*.db
    # ipsec initnss
    # ipsec newhostkey --output libre.secrets 2>&1 | tail -n1 | awk '{printf("%s", $7)}'
    openssl pkcs12 -export -in $main/crypto/$name.crt \
        -inkey $main/crypto/$name.key -certfile $shared/crypto/ca.crt \
        -out $main/crypto/$name.p12 -name $name -password pass:

    # ipsec import $main/crypto/$name.p12
    # pk12util -i $main/crypto/$name.p12 -d sql:/var/lib/ipsec/nss -K "" \
    #     -W ""
    $sudo pk12util -i $main/crypto/$name.p12 -d sql:/etc/ipsec.d -K "" \
        -W ""

    cat > $main/conf/libreswan/ipsec.secrets << EOF
: RSA "$name"
EOF

    $sudo cp $main/conf/libreswan/ipsec.{conf,secrets} /etc/
    $sudo cp $main/conf/libreswan/libre.conf /etc/ipsec.d

elif [ "$software" = "openswan" ]; then
    cat > $main/conf/openswan/ipsec.secrets << EOF
: RSA $name.key
EOF

    $sudo cp $main/conf/openswan/ipsec.{conf,secrets} /etc/
    $sudo cp $shared/crypto/ca.crt /etc/ipsec.d/cacerts/
    $sudo cp $main/crypto/$name.key /etc/ipsec.d/private
    $sudo cp $main/crypto/$name.crt /etc/ipsec.d/certs
fi
