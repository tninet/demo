#!/usr/bin/env bash

# exec &> /dev/null
# This is needed for Libreswan installation to not stop and
# ask us things
export DEBIAN_FRONTEND=noninteractive

url="https://download.libreswan.org/binaries"
url="$url/ubuntu/15.04/libreswan_3.15-1_amd64.deb"
if [ "$software" = "strongswan" ] || \
    [ "$software" = "openswan" ]
then
    $sudo apt-get install -y $software
elif [ "$software" = "libreswan" ]; then
    cd $shared
    if [ ! -f "libreswan_3.15-1_amd64.deb" ]; then
        wget $url
    fi
    cd
    cp $shared/libreswan_3.15-1_amd64.deb .
    $sudo dpkg -i ./libreswan_3.15-1_amd64.deb || true
    $sudo apt-get install -f -y
fi

if [ "$software" = "libreswan" ]; then
    $sudo sed -i '/ExecReload/a TimeoutStopSec=5' \
        /lib/systemd/system/ipsec.service
    # echo "TimeoutStopSec=5" >> \
        # /lib/systemd/system/ipsec.service
    # systemctl set-property ipsec.service TimeoutStopSec=4
elif [ "$software" = "strongswan" ]; then
    $sudo sed -i '/ExecStopPost/a TimeoutStopSec=5' \
        /lib/systemd/system/strongswan.service
    $sudo systemctl daemon-reload
fi
