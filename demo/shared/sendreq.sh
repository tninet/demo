if [ "$software" = "libreswan" ] || \
    [ "$software" = "openswan" ]; then
    infix="auto --"
else
    infix=""
fi

upcmd() {
    ipsec ${infix}up $1
    # swanctl --initiate $@
}
downcmd() {
    # ipsec ${infix}down $@
    :
}

# export -f downcmd

nullfunc() { :; }

# Sends an m1 message. If this does not lead to a new
# connection after $tacc, stops sending any m1 or m3
# messages (no retransmission). Otherwise, leaves the
# connection up.
sendreq () {
    local tfail="${1:-$tacc}"
    local conn="${2:-conn1}"
    # Names of the functions to execute
    successaction=${3:-nullfunc}
    failaction=${4:-nullfunc}
    export -f upcmd
    export infix
    export conn
    # For Libreswan tr will always equal tfail because even
    # after sending a AUTH_FAILED notification Libreswan's
    # command ipsec auto --up does not terminate. Therefore,
    # we currently cannot measure the Initiators's response
    # time for Libreswan.
    set +e
    trap - ERR
    timeresult=$(/usr/bin/time -p timeout -s 9 $tfail \
        bash -c "upcmd $conn > /dev/null 2>&1" 2>&1)
    ret=$?
    tr=$(echo "$timeresult" | grep -e ^real | cut -d ' ' -f 2)
    set -e
    trap traperr ERR

    if [ $ret -eq 137 ]; then
        $failaction
        if [ "$software" = "strongswan" ]; then
            # The Child SA has not been created yet. There
            # is only one half-open IKE SA whose name is
            # conn1[${conn#conn}]. We thus should use:
            # downcmd $conn
            # However in some cases the IKE SA's name is
            # conn1[n] with n different from ${conn#conn}.
            # Therefore we should use:
            downcmd conn1 2>&1
            return 0
        else
            # Check if the same problem as above arises.
            downcmd "$conn" 2>&1
            return 0
        fi
    elif [ $ret -eq 0 ]; then
        $successaction
        return 0
    else
        yell "Failed to send an m1 message for $conn"
        exit 1
    fi
}
