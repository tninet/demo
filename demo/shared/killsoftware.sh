#!/usr/bin/env bash

# exec &> /dev/null

if [ "$software" = "strongswan" ]; then
    if st="$($sudo timeout 5 ipsec status)"; then
        head -n 1 <<< "$st" >> $log
    fi

    if ! $sudo timeout 15 systemctl stop strongswan.service
    then
        echo "killing $software with force"
        $sudo pkill -f -9 charon || true
        $sudo pkill -f -9 starter || true
        $sudo pkill -f -9 stroke || true
    fi
    $sudo timeout 5 ipsec stop &> /dev/null || true
elif [ "$software" = "libreswan" ]; then
    # if st="$($sudo timeout 5 ipsec whack --trafficstatus)"
    # then
        # echo "$st" >> $log
    # fi
    $sudo systemctl stop ipsec.service
elif [ "$software" = "openswan" ]; then
    echo """

    ipsec status:

    """ >> $log
    $sudo timeout 2s ipsec auto --status >> $log
    $sudo timeout 5s service ipsec stop
    if [ ! $? ]; then
        echo "killing $software with force"
        $sudo pkill -f -9 "ipsec/pluto"
    fi
fi
