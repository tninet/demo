name="probe"

if [ ! -d "/vagrant" ]; then
    source "../shared/pre.sh"
else
    source "/vagrantshared/pre.sh"
fi

daemonlog="/vagrant/log/$daemon.log"
succlog="/vagrant/log/success.log"
trlog="/vagrant/log/tr.log"
tmpfile="/dev/shm/detected"
